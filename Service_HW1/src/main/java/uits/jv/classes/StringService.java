/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.classes;

/**
 *
 * @author yulia
 */
public class StringService {

    private String[][] SpeechStorage = {{"Дорогие друзья! ", "с другой стороны ",
        "равным образом ", "Не следует, однако, забывать о том, что ", "Таким "
        + "образом, ", "Повседневная практика показывает, что ", "Значимость "
        + "этих проблем настолько очевидна, что ", "Разнообразный и богатый "
        + "опыт ", "Задача организации, в особенности же ", "Соображения "
        + "высшего порядка, а также "}, {"реализация намеченных плановых "
        + "заданий ", "рамки и место обучения кадров ", "постоянный количественный "
        + "рост и сфера нашей активности ", "сложившаяся структура организации ",
        "новая модель ", "организационной деятельности ", "дальнейшее развитие "
        + "различных форм деятельности ", "постоянное информационно-пропагандистское "
        + "обеспечение нашей деятельности ", "укрепления и развития структуры ",
        "консультация с широким активом ", "начало повседневной работы по "
        + "формированию позиции "}, {"играет важную роль в формировании ",
        "требуют от нас анализа ", "требуют определения и уточнения ",
        "способствует подготовке и реализации ", "обеспечивает широкому "
        + "кругу специалистов участие в формировании ", "позволяет выполнить",
        "важнейшие задания по разработке ", "в значительной степени обуславливает "
        + "создание ", "позволяет оценить значение ", "представляет собой "
        + "интересный эксперимент проверки ", "влечет за собой процесс внедрения "
        + "и модернизации "}, {"существующих финансовых и административных условий ",
        "дальнейших направлений развитая ", "системы массового участия ",
        "позиций, занимаемых участниками в отношении поставленных задач ",
        "новых предложений ", "направлений прогрессивного развития ",
        "системы обучения кадров ", "соответствующей насущным потребностям ",
        "соответствующих условий активизации ", "модели развития ", "форм воздействия "}};

    public String generateSpeech() {
        
        StringBuilder speech = new StringBuilder();
        for (int i = 0; i < 50; i++) {
            int randomFirst = (int) (Math.random() * 10);
            int randomSecond = (int) (Math.random() * 10);
            int randomThird = (int) (Math.random() * 11);
            int randomFourth = (int) (Math.random() * 10);
            speech.append(SpeechStorage[0][randomFirst]).append(SpeechStorage[1][randomSecond]).append(SpeechStorage[2][randomThird]).append(SpeechStorage[3][randomFourth]).append(". \n");
        }
        return speech.toString();

    }
    

}
