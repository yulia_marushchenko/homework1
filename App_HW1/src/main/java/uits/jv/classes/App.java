/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.classes;

import uits.js.classes.StringUtil;

/**
 *
 * @author yulia
 */
public class App {
    public static void main(String[] args) {
        
        StringService service = new StringService();
        StringUtil util = new StringUtil();
        
        String text = service.generateSpeech();
        System.out.println(text);
        System.out.println("Text is empty: " + util.isEmpty(text));
        System.out.println(util.findAndReplace(text, "а", "QQQQQ"));
        
    }
}
