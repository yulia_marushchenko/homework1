/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.js.classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author yulia
 */
public class StringUtil {

    public boolean isEmpty(final String text) {

        boolean isEmptyString;
        if (text != null) {
            isEmptyString = text.isEmpty();
        } else {
            isEmptyString = true;
        }
        return isEmptyString;

    }

    public String findAndReplace(String text, String wordOfReplacement, String wordForReplacement) {

        String regex = wordOfReplacement;
        String workingText = text;
        String newWord = wordForReplacement;
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(workingText);
        if (m.find()) {
            workingText = m.replaceAll(newWord);
        }
        return workingText;
    }


}
